## emmet语法记录

### 子元素Child

> 语法

```console
nav>ul>li*3{$$$}
```

> 效果

```html
<nav>
    <ul>
        <li>001</li>
        <li>002</li>
        <li>003</li>
    </ul>
</nav>
```

> [01-Syntax-Child]("./01-Syntax-Child.html")

### 兄弟节点Sibling

> 语法

```emmet
div+p+bq
```

> 效果

```html
<div></div>
<p></p>
<blockquote></blockquote>
```

> [02-Syntax-Sibling]("./02-Syntax-Sibling.html")

### 节点爬升ClimbUp

- 同级爬升


> 语法

```console
div+div>p>span+em+bq
```

> 效果

```html
<div></div>
<div>
    <p>
        <span></span>
        <em></em>
        <blockquote></blockquote>
    </p>
</div>
```

- 爬升一级

> 语法

```console
div+div>p>span>em^bq 
```

> 效果

```html
<div></div>
<div>
    <p>
        <span>
            <em></em>
        </span>
        <blockquote></blockquote>
    </p>
</div>
```

- 爬升二级

> 说法

```console 
div+div>p>span>em^^bq
```
> 效果

```html
<div></div>
<div>
    <p>
        <span>
            <em></em>
        </span>
    </p>
    <blockquote></blockquote>
</div>
```
- 爬升三级

> 语法

```console 
div+div>p>span>em^^bq
```

> 效果

```html
<div></div>
<div>
    <p>
        <span>
            <em></em>
        </span>
    </p>
</div>
<blockquote></blockquote>
```

[03-Syntax-Climb-Up]("./03-Syntax-Climb-Up.html")

### 分组Grouping

- 示例1

--------------------------------------------

> 语法

```console
div>(header>ul>li*2>a)+(section>p*3{$$$})+footer>p
```

> 效果

```html
<div>
    <header>
        <ul>
            <li>
                <a href=""></a>
            </li>
            <li>
                <a href=""></a>
            </li>
        </ul>
    </header>
    <section>
        <p>001</p>
        <p>002</p>
        <p>003</p>
    </section>
    <footer>
        <p></p>
    </footer>
</div>
```



- 示例2

----------------------------------------
> 语法

```console
(div>dl>dt{$$$}*6)+footer>p
```

> 效果

```html
<div>
    <dl>
        <dt>001</dt>
        <dt>002</dt>
        <dt>003</dt>
        <dt>004</dt>
        <dt>005</dt>
        <dt>006</dt>
    </dl>
</div>
<footer>
    <p></p>
</footer>
```

> [04-Syntax-Grouping]("./04-Syntax-Grouping.html")

### 乘法Multiplication

> 语法

```console
ul>li{$$$$$}*6
```

> 效果

```html
<ul>
    <li>00001</li>
    <li>00002</li>
    <li>00003</li>
    <li>00004</li>
    <li>00005</li>
    <li>00006</li>
</ul>
```

> [05-Syntax-Multiplication]("./05-Syntax-Multiplication.html")

### 项目编号ItemNumbering

- 示例1

> 语法

```console
ul>li.item$*6{$$$$$}
```

> 效果

```html
<ul>
    <li class="item1">00001</li>
    <li class="item2">00002</li>
    <li class="item3">00003</li>
    <li class="item4">00004</li>
    <li class="item5">00005</li>
    <li class="item6">00006</li>
</ul>
```

- 示例2

> 语法

```console
h$[title=item$]{Header $}*3
```

> 效果

```html
<h1 title="item1">Header 1</h1>
<h2 title="item2">Header 2</h2>
<h3 title="item3">Header 3</h3>
```

- 示例3

```console
h$[data-title=data-title$ data-name=data-name$]{Header $}*3
```

> 效果

```html
<h1 data-title="data-title1" data-name="data-name1">Header 1</h1>
<h2 data-title="data-title2" data-name="data-name2">Header 2</h2>
<h3 data-title="data-title3" data-name="data-name3">Header 3</h3>
```

- 示例4

> 语法

```console
ul>li.item$@-{中华人民共和国$@-}*5
```
> 效果

```html
<ul>
    <li class="item5">中华人民共和国5</li>
    <li class="item4">中华人民共和国4</li>
    <li class="item3">中华人民共和国3</li>
    <li class="item2">中华人民共和国2</li>
    <li class="item1">中华人民共和国1</li>
</ul>
```

- 示例5

> 语法

```console
    ul>li.item${中华人民共和国$@-}*5
```

> 效果

```html
<ul>
    <li class="item1">中华人民共和国5</li>
    <li class="item2">中华人民共和国4</li>
    <li class="item3">中华人民共和国3</li>
    <li class="item4">中华人民共和国2</li>
    <li class="item5">中华人民共和国1</li>
</ul>
```

- 示例6

> 语法

```console
ul>li.item$@-{中华人民共和国$}*5
```

> 效果

```html
<ul>
    <li class="item5">中华人民共和国1</li>
    <li class="item4">中华人民共和国2</li>
    <li class="item3">中华人民共和国3</li>
    <li class="item2">中华人民共和国4</li>
    <li class="item1">中华人民共和国5</li>
</ul>
```

- 示例7

> 语法

```console
ul>li.item$@{中华人民共和国$@6}*5
```
> 效果

```html
<ul>
    <li class="item1">中华人民共和国6</li>
    <li class="item2">中华人民共和国7</li>
    <li class="item3">中华人民共和国8</li>
    <li class="item4">中华人民共和国9</li>
    <li class="item5">中华人民共和国10</li>
</ul>
```

- 示例8

> 语法

```console
ul>li.item$@{中华人民共和国$@-6}*5
```

> 效果

```html
<ul>
    <li class="item1">中华人民共和国10</li>
    <li class="item2">中华人民共和国9</li>
    <li class="item3">中华人民共和国8</li>
    <li class="item4">中华人民共和国7</li>
    <li class="item5">中华人民共和国6</li>
</ul>
```

> [06-Syntax-Item-Numbering]("./06-Syntax-Item-Numbering.html")

